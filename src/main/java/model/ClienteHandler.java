package model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Objects;


public class ClienteHandler implements Runnable {
    private Socket socket;
    private String codigoEncerrarChat = "->ENCERRAR CHAT-<";
    private MensagemListener mensagemListener;

    public void adicionarMensagemListener(MensagemListener listener) {
        this.mensagemListener = listener;
    }
    public ClienteHandler(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);

            String mensagem;
            while ((mensagem = reader.readLine()) != null) {

                // Separa a mensagem entre o usuário que enviou e a mensagem
                String[] mensagemSeparada = mensagem.split("/:/");



                String mensagemFinal = mensagemSeparada[0] + ": " + mensagemSeparada[3];


                String nomeUsuario = mensagemSeparada[0];
                StatusUsuario statusUsuario = StatusUsuario.valueOf(mensagemSeparada[1]);
                String[] usuarioEndereco = mensagemSeparada[2].split("/");
                InetAddress inetAddress = InetAddress.getByName(usuarioEndereco[1]);

                Usuario usuarioRemetente = new Usuario(nomeUsuario, statusUsuario, inetAddress);


                System.out.println(mensagemSeparada[3]);
                if(Objects.equals(mensagemSeparada[3], codigoEncerrarChat))
                {
                    //fechar tab do chat
                    System.out.println("Fechar tab do chat");
                    String mensagemFechar = nomeUsuario + " Encerrou chat";
                    mensagemListener.onMensagemRecebida(usuarioRemetente, this.socket, mensagemFechar, true);
                }
                else{
                    mensagemListener.onMensagemRecebida(usuarioRemetente, this.socket, mensagemFinal, false);
                }
            }
        } catch (IOException e) {
            System.out.println("CLiente Handler: socket fechado");
        }
        finally {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
