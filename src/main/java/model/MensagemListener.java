package model;

import java.io.IOException;
import java.net.Socket;

public interface MensagemListener {
    void onMensagemRecebida(Usuario remetente, Socket socket, String mensagem, boolean fechar) throws IOException;
}