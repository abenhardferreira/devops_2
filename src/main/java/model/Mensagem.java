package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Mensagem {

    private String tipo;
    private String usuario;
    private String status;


    @Override
    public String toString() {
        return "Mensagem{" +
                "tipo='" + tipo + '\'' +
                ", usuario='" + usuario + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
