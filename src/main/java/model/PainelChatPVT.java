package model;

import lombok.Getter;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

@Getter
public class PainelChatPVT extends JPanel {
    JTextArea areaChat;
    JTextField campoEntrada;
    Usuario meuUsuario;
    Usuario usuarioDestino;
    Socket socket;
    private String mensagem;
    private PrintWriter writer;


    public PainelChatPVT(Usuario meuUsuario, Usuario usuarioDestino, Socket socket) {
        this.meuUsuario = meuUsuario;
        this.usuarioDestino = usuarioDestino;
        this.socket = socket;

        try {
            // Inicialize o PrintWriter usando o OutputStream do socket
            this.writer = new PrintWriter(socket.getOutputStream(), true);
        } catch (IOException e) {
            System.out.println("print writer esta null");
            e.printStackTrace();
        }

        //cria a interface do painel de chat
        setLayout(new GridBagLayout());
        areaChat = new JTextArea();
        areaChat.setEditable(false);
        campoEntrada = new JTextField();


        //campo de escrita
        campoEntrada.addActionListener(e -> {
            mensagem = e.getActionCommand();

            //verifica se a mensagem esta vazia, se sim faz nada
            if (!mensagem.isEmpty()) {
                // formata a mensagem
                String mensagemFormatada = meuUsuario.getNome() + "/:/" + meuUsuario.getStatus() + "/:/" + meuUsuario.getEndereco()+ "/:/" +mensagem;
                // Envia a mensagem para o destinatário via PrintWriter associado ao Socket
                writer.println(mensagemFormatada);
                System.out.println("Enviou mensagem: " + mensagemFormatada);
                // Limpa o campo de entrada após o envio da mensagem
                campoEntrada.setText("");
                SwingUtilities.invokeLater(() -> {
                    areaChat.append(meuUsuario.getNome() + ": " + mensagem+ "\n");
                });
            }
        });
        add(new JScrollPane(areaChat), new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        add(campoEntrada, new GridBagConstraints(0, 1, 1, 1, 1, 0, GridBagConstraints.SOUTH, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

    }

    public void startChat() throws IOException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()))) {

            String mensagem;
            while (!socket.isClosed()) {
                mensagem = reader.readLine();
                if (mensagem == null) {
                    break;
                }
                String[] mensagemSeparadaArray = mensagem.split("/:/");
                String mensagemSeparada = mensagemSeparadaArray[0] + ": " + mensagemSeparadaArray[3] + "\n";

                SwingUtilities.invokeLater(() -> {
                    areaChat.append(mensagemSeparada);
                });
            }
        } catch (IOException e) {
            if (!socket.isClosed()) {
                System.out.println("Socket fechado");
                e.printStackTrace();
            }
        }
    }
    public void adicionarMensagem(String mensagem) {
        System.out.println("adicionar Mensagem: " + mensagem);
        SwingUtilities.invokeLater(() -> {
            areaChat.append(mensagem + "\n");
        });
    }
    public void closeChat() throws IOException {
        writer.close();
        socket.close();
    }
    public void destroyPanel() {
        Container parent = getParent();
        if (parent != null) {
            parent.remove(this);
            parent.revalidate();
            parent.repaint();
        }
    }
}