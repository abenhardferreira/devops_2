package sondas;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Setter;
import model.Mensagem;
import model.StatusUsuario;
import model.Usuario;

import javax.swing.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@Setter
@AllArgsConstructor
public class RecebeSonda implements Runnable {

    private Map<Usuario, Long> usuariosAtivos;
    private Usuario meuUsuario;
    private DefaultListModel<Usuario> dfListModel;


    @Override
    public void run() {
        try (DatagramSocket socket = new DatagramSocket(8084)) {
            ObjectMapper om = new ObjectMapper();
            while (true) {
                byte[] buf = new byte[1024];
                DatagramPacket packet = new DatagramPacket(buf, buf.length);
                socket.receive(packet);
                Mensagem sonda = om.readValue(buf, 0, packet.getLength(), Mensagem.class);
               if (!sonda.getUsuario().equals(meuUsuario.getNome())) {

                   System.out.println("recebeu sonda:" + sonda);

                    int idx = dfListModel.indexOf(new Usuario(sonda.getUsuario(), StatusUsuario.valueOf(sonda.getStatus()), packet.getAddress()));
                    if (idx == -1) {
                        Usuario usuario = new Usuario(sonda.getUsuario(), StatusUsuario.valueOf(sonda.getStatus()), packet.getAddress());
                        SwingUtilities.invokeLater(() -> {
                            dfListModel.addElement(usuario);

                            usuariosAtivos.put(usuario, System.currentTimeMillis());
                        });
                    } else {
                        Usuario usuario = dfListModel.getElementAt(idx);
                        usuario.setStatus(StatusUsuario.valueOf(sonda.getStatus()));
                        usuariosAtivos.replace(usuario, System.currentTimeMillis());
                        dfListModel.setElementAt(usuario, idx);
                    }
               }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}