package sondas;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Setter;
import model.Mensagem;
import model.Usuario;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.concurrent.TimeUnit;

@Setter
public class EnviaSonda implements Runnable {

    private Usuario usuario;
    private String ipDaRede = "192.168.100.";

    public EnviaSonda(Usuario usuario){this.usuario =usuario;}

    @Override
    public void run() {
        synchronized (this) {
            if (usuario == null) {
                try {
                    this.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        try (DatagramSocket socket = new DatagramSocket()) {
            while (true) {
                Mensagem mensagem = new Mensagem("sonda", usuario.getNome(), usuario.getStatus().toString());
                ObjectMapper om = new ObjectMapper();
                byte[] msgJson = om.writeValueAsBytes(mensagem);

                for (int n = 1; n < 255; n++) {
                    DatagramPacket packet = new DatagramPacket(msgJson, msgJson.length, InetAddress.getByName(ipDaRede + n), 8084);
                    socket.send(packet);
                }
                TimeUnit.SECONDS.sleep(5);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}