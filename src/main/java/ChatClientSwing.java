import model.*;
import sondas.EnviaSonda;
import sondas.RecebeSonda;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.Time;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class ChatClientSwing extends JFrame implements MensagemListener {
    private String nomeUsuario;
    private Usuario meuUsuario;
    private final long tempoParaTimeoutPorInatividade = 30000;
    private ServerSocket serverSocket;
    private Map<Usuario, Long> usuariosAtivos = new ConcurrentHashMap<>();
    private JList<Usuario> listaChat;
    private DefaultListModel<Usuario> dfListModel;
    private JTabbedPane tabbedPane = new JTabbedPane();
    private Set<Usuario> chatsAbertos = new HashSet();
    private Map<Usuario, PainelChatPVT> openChatPanels = new ConcurrentHashMap<>();

    public static void main(String[] args) throws UnknownHostException {
        new ChatClientSwing();
    }

    public ChatClientSwing() throws UnknownHostException {

        setLayout(new GridBagLayout());
        do {
            nomeUsuario = JOptionPane.showInputDialog(this, "Digite seu nome de usuário: ");
        }while(nomeUsuario == null);

        synchronized (this) {
            this.meuUsuario = new Usuario(nomeUsuario, StatusUsuario.DISPONIVEL, InetAddress.getLocalHost());
            this.notify();
        }
        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("Status");

        ButtonGroup group = new ButtonGroup();
        ActionListener statusChangeListener = e -> {
            StatusUsuario novoStatus = StatusUsuario.valueOf(e.getActionCommand());
            ChatClientSwing.this.meuUsuario.setStatus(novoStatus);
        };

        for (StatusUsuario status : StatusUsuario.values()) {
            JRadioButtonMenuItem rbMenuItem = new JRadioButtonMenuItem(status.name());
            rbMenuItem.addActionListener(statusChangeListener);
            group.add(rbMenuItem);
            menu.add(rbMenuItem);
        }

        menuBar.add(menu);
        this.setJMenuBar(menuBar);

        //Evento de fechar o chat com o botão direito do mouse
        tabbedPane.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
                if (e.getButton() == MouseEvent.BUTTON3) {
                    int tabIndex = tabbedPane.indexAtLocation(e.getX(), e.getY());
                    if (tabIndex != -1) {
                        JPopupMenu popupMenu = new JPopupMenu();
                        JMenuItem item = new JMenuItem("Fechar");
                        item.addActionListener(e1 -> {

                            Component component = tabbedPane.getComponentAt(tabIndex);
                            if (component instanceof PainelChatPVT) {
                                PainelChatPVT painel = (PainelChatPVT) component;
                                Usuario usuario = painel.getUsuarioDestino();

                                //fecha a tab/chat
                                if (painel.getSocket() != null && !painel.getSocket().isClosed()) {
                                    PrintWriter writer;
                                    try {
                                        writer = new PrintWriter(painel.getSocket().getOutputStream(), true);
                                        String mensagem = meuUsuario.getNome() + "/:/" + meuUsuario.getStatus() + "/:/" + meuUsuario.getEndereco()+ "/:/" + "->ENCERRAR CHAT-<";
                                        writer.println(mensagem);
                                        painel.closeChat();
                                    } catch (IOException ex) {
                                        throw new RuntimeException(ex);
                                    }
                                }

                                SwingUtilities.invokeLater(() -> {
                                    tabbedPane.removeTabAt(tabIndex);
                                    chatsAbertos.remove(usuario);
                                    openChatPanels.remove(usuario);
                                });
                            }
                        });

                        popupMenu.add(item);
                        popupMenu.show(e.getComponent(), e.getX(), e.getY());
                    }
                }
            }
        });



        add(new JScrollPane(criaLista()), new GridBagConstraints(0, 0, 1, 1, 0.1, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        add(tabbedPane, new GridBagConstraints(1, 0, 1, 1, 1, 1, GridBagConstraints.EAST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));




        setSize(800, 600);
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        final int x = (screenSize.width - this.getWidth()) / 2;
        final int y = (screenSize.height - this.getHeight()) / 2;
        this.setLocation(x, y);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


        setVisible(true);

        new Thread(new EnviaSonda(meuUsuario)).start();
        new Thread(new RecebeSonda(usuariosAtivos, meuUsuario, dfListModel)).start();

        Servidor();
        //° Thread para verificar usuários inativos a cada segundo
        new Thread(() -> {
            while (true) {
                removerUsuarioInativo();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();



    }
    private JComponent criaLista() {
        dfListModel = new DefaultListModel<>();
        listaChat = new JList<>(dfListModel);

        //evento de clickar duas vezes no nome do usuário na lista
        listaChat.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {
                if (evt.getClickCount() == 2) {
                    int index = listaChat.locationToIndex(evt.getPoint());
                    Usuario usuarioRemetente = listaChat.getModel().getElementAt(index);

                    // Adicionar o código para abrir automaticamente uma aba para o usuário clicado
                    if (!chatsAbertos.contains(usuarioRemetente)) {
                        abreJanelaChat(usuarioRemetente);
                    }

                }
            }
        });
        return listaChat;
    }
    private void abreJanelaChat(Usuario usuarioDestino) {
        try {

            if (!openChatPanels.containsKey(usuarioDestino)) {

                //cria um socket
                Socket socket = new Socket(usuarioDestino.getEndereco(), 8085);

                criaPainelChat(usuarioDestino, socket, null);
            } else {
                // se a tab já existe abre ela
                tabbedPane.setSelectedComponent(openChatPanels.get(usuarioDestino));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void fechaJanelaChat(PainelChatPVT painel, Usuario usuario) {
        if (painel != null && painel.getSocket() != null && !painel.getSocket().isClosed()) {
            try {
                painel.adicionarMensagem("Chat foi encerrado pelo usuário " + usuario.getNome());
                painel.closeChat();
                System.out.println("Fechando chat");
                Timer timer = new Timer(3000, new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        SwingUtilities.invokeLater(() -> {
                            synchronized (tabbedPane) {
                                tabbedPane.remove(painel);
                            }
                            synchronized (chatsAbertos) {
                                chatsAbertos.remove(usuario);
                            }
                            painel.destroyPanel();
                            synchronized (openChatPanels) {
                                openChatPanels.remove(usuario);

                                System.out.println("timer acionado");
                            }
                        });
                    }
                });
                timer.setRepeats(false);
                timer.start();
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
    }



    private void criaPainelChat(Usuario usuarioDestino, Socket socket, String mensagem) {
        //abre uma painel de chat com o usuário
        PainelChatPVT chatPanel = new PainelChatPVT(meuUsuario, usuarioDestino, socket);

        //cria a tab com o nome daquele usuario
        synchronized (tabbedPane) {
            tabbedPane.add(usuarioDestino.toString(), chatPanel);
        }
        //adiciona na lista de chats e painéis abertos
        synchronized (chatsAbertos) {
            chatsAbertos.add(usuarioDestino);
        }
        synchronized (openChatPanels) {
            openChatPanels.put(usuarioDestino, chatPanel);
        }
        if(mensagem !=null){
            chatPanel.adicionarMensagem(mensagem);
        }

        //cria a thread para o chat
        new Thread(() -> {
            try {
                chatPanel.startChat();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }


    //Remoção de usuário inativo
    private void removerUsuarioInativo() {

        long tempoAtual = System.currentTimeMillis();
        Set<Usuario> usuariosInativos = new HashSet<>();

            //verifica o usuario e o tempo de sua ultima sonda
            for (Map.Entry<Usuario, Long> usuario : usuariosAtivos.entrySet()) {
                //se o tempo atual menos o tempo da ultima sonda pelo usuário for maior que 30 segundos ou o tempoParaTimeoutPorInatividade
                //usuario é posto em uma lsita de inativos
                if (tempoAtual - usuario.getValue() > tempoParaTimeoutPorInatividade) {
                    usuariosInativos.add(usuario.getKey());
                }
            }

            //varre a lista de inativos e os remove das demais listas
            if(!usuariosInativos.isEmpty()) {
                for (Usuario usuario : usuariosInativos) {

                    //se o usuário inativo está em sua list chats deve ser removido e o chat fechado
                    if (chatsAbertos.contains(usuario)) {
                        synchronized (chatsAbertos) {
                            chatsAbertos.remove(usuario);
                        }
                        synchronized (openChatPanels) {
                            openChatPanels.remove(usuario);
                        }
                    }

                    //remove usuário da lista default e de usuários ativos
                    synchronized (dfListModel) {
                        dfListModel.removeElement(usuario);
                    }
                    synchronized (usuariosAtivos) {
                        usuariosAtivos.remove(usuario);
                    }
                }
            }
    }
    private void Servidor() {
        try {
            serverSocket = new ServerSocket(8085);
            new Thread(() -> {
                try {
                    while (true) {
                        Socket clientSocket = serverSocket.accept();

                        System.out.println("Conectado com usuario");
                        // Adiciona um novo ClienteHandler para lidar com a conexão
                        ClienteHandler clienteHandler = new ClienteHandler(clientSocket);
                        clienteHandler.adicionarMensagemListener(this); // Registra ChatClientSwing como ouvinte
                        // Inicia uma nova thread para o ClienteHandler
                        new Thread(clienteHandler).start();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }).start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onMensagemRecebida(Usuario remetente, Socket socket, String mensagem, boolean fechar) {
        if (fechar) {
            System.out.println("ChatClient onMensagem Fechando chat, usuario remetente:" + remetente + " mensagem: " + mensagem);
            PainelChatPVT painel = openChatPanels.get(remetente);
            painel.adicionarMensagem( remetente.getNome() + " fechou o chat");
            fechaJanelaChat(painel, remetente);
        } else if (!chatsAbertos.contains(remetente)) {
            criaPainelChat(remetente, socket, mensagem);
        } else {
            openChatPanels.get(remetente).adicionarMensagem(mensagem);
        }
    }
}

