FROM openjdk:17

# Instalação do xvfb
RUN apt-get update \
    && apt-get install -y xvfb \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /app

COPY target/Chat_P2P-1.0-SNAPSHOT.jar /app/Chat_P2P.jar

# Configuração e execução do Xvfb e do Java
CMD ["sh", "-c", "Xvfb :99 -ac -screen 0 1024x768x16 && export DISPLAY=:99 && java -jar Chat_P2P.jar"]
